# Project structure

The project is configured so that any `.cpp` files in the `src` directory will be included in the build.

## To build

1. Run CMake. From the project root
```
$ mkdir build && cd build
$ cmake ..
```

2. Run make
```
$ make
```

3. Run application
```
$ ./src/MyProject
```

4. Subsequently you only need to rerun `make`

